package program
import core._
import parser._
import scala.collection.immutable.HashMap
import scala.collection.immutable.ListMap

object Main {
  var isConstant : Map[String, Boolean] = new HashMap[String, Boolean]
  var allVariables : Map[String, Double] = new HashMap[String, Double]
  var allFunctions : Map[String, (String, Function)] = new HashMap[String, (String, Function)]
  val parser: Parser = new Parser()
  val evaluator: Evaluator = new Evaluator()
  def getVariable(expr:String) : Unit = {
    val newVar = parser.parse(parser.vardef, expr)
    val newVarVal = evaluator.evaluate(newVar.get._2, allVariables, allFunctions)
    val newVarName = newVar.get._1
    allVariables = allVariables + (newVarName->newVarVal)   
  }
  def getFunction(expr:String) : Unit = {
    val newFunc = parser.parse(parser.funcdef, expr)
    allFunctions = allFunctions + (newFunc.get._1->newFunc.get._2)
  }
  def getExpressionValue(expr:String) : Double = {
    val exprFunc = parser.parse(parser.expression, expr)
    evaluator.evaluate(exprFunc.get, allVariables, allFunctions)
  }
  def main(args : Array[String]) : Unit = {
    getVariable("x = cos(3.14)")
    getFunction("f(y) = x + y")
    getFunction("r(z) = f(z) * (1)")
    val value = getExpressionValue("r(x)")
    println(value)
    
    val integrate_func = new UnaryFunction(new VariableFunction("integrate_arg"), (x)=>x+1)
    val integrator = (getVal: Double=>Double, leftLimit : Double, rightLimit : Double) => {
      (getVal(leftLimit) + getVal(rightLimit))/2
    }
    val integrate_provider = new IntegratorFunction(integrate_func, new ConstantFunction(1), new ConstantFunction(2), integrator) 
    val value2 = evaluator.evaluate(integrate_provider, allVariables, allFunctions)
    println(value2)
    
  }
}