package parser

import scala.util.parsing.combinator._
import scala.util.matching.Regex
import core._

class Parser extends RegexParsers{
  def expression = operation1

  def vardef: Parser[(String,Function)] = new Regex("[a-z]+")~"="~expression ^^ {
    case a~b~c => (a.toString(), c)
  }

  def funcdef: Parser[(String, (String, Function))] = new Regex("[a-z]+")~"("~new Regex("[a-z]+")~")"~"="~expression ^^ {
    case x~a~y~b~c~z => (x.toString(), (y.toString(), z))
  }
  
  def number: Parser[Function] = new Regex("(\\+|\\-)?")~ new Regex("([0-9]*[.]?[0-9]+|[0-9]+[.]?[0-9]*)") ^^ {
    case "+"~s => new ConstantFunction(s.toDouble)
    case "-"~s => new ConstantFunction(-s.toDouble)
    case ""~s => new ConstantFunction(s.toDouble)
  }
  
  def variable: Parser[Function] = new Regex("[a-z]+") ^^ {
    case s => new VariableFunction(s)
  }
  
  def brackets: Parser[Function] = "("~operation1~")" ^^ {
    case a~x~b => x
  }
  
  def func: Parser[Function] = new Regex("[a-z]+") ~ "(" ~ operation1 ~ ")" ^^ {
    case "sin"~a~y~b => new UnaryFunction(y,scala.math.sin)
    case "cos"~a~y~b => new UnaryFunction(y,scala.math.cos)
    case x~a~y~b => new UserFunction(x, y)
  }
  def ident: Parser[Function] = brackets | func | variable | number
  
  def operation1: Parser[Function] = operation2 ~ rep(new Regex("\\+|\\-") ~ operation2) ^^ {
    case op ~ list => list.foldLeft(op) {
      case (x, "+" ~ y) => new BinaryFunction(x, y, (a,b)=>a+b)
      case (x, "-" ~ y) => new BinaryFunction(x, y, (a,b)=>a-b)
    }
  }
  def operation2: Parser[Function] = ident ~ rep(new Regex("\\*|/") ~ ident) ^^ {
    case op ~ list => list.foldLeft(op) {
      case (x, "*" ~ y) => new BinaryFunction(x, y, (a,b)=>a*b)
      case (x, "/" ~ y) => new BinaryFunction(x, y, (a,b)=>a/b)
    }
  }
}