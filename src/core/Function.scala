package core

trait Function
case class ConstantFunction(val value: Double) extends Function
case class VariableFunction(val name: String) extends Function
case class UserFunction(val name: String, val variableValue: Function) extends Function
case class UnaryFunction(val argument: Function, val func: Double=>Double) extends Function
case class BinaryFunction(func1: Function, func2: Function, val toApply:(Double, Double)=>Double) extends Function
case class IntegratorFunction(func: Function, a: Function, b: Function, method:(Double=>Double, Double, Double)=>Double) extends Function
