package core

class Evaluator {
  def evaluate (f : Function, variables : Map[String, Double], functions: Map[String, (String, Function)]) : Double = f match {
    case ConstantFunction(value) => value
    case VariableFunction(name) => variables.get(name).get
    case BinaryFunction(f1, f2, toApply) => toApply(evaluate(f1, variables, functions), evaluate(f2, variables, functions))
    case UnaryFunction(f1, toApply) => toApply(evaluate(f1, variables, functions))
    case UserFunction(funcName, argFunc) => {
      val func = functions.get(funcName)
      val argVal = evaluate(argFunc, variables, functions)
      val newVariables = variables + (func.get._1->argVal)
      val newFunctions = functions - funcName
      evaluate(func.get._2, newVariables, newFunctions)
    }
    case IntegratorFunction(func, a, b, method) => {
      val leftLimit = evaluate(a, variables, functions)
      val rightLimit = evaluate(b, variables, functions)
      def valueGetter = (point:Double)=> {
        val newVariables = variables + ("integrate_arg"->point)
        evaluate(func, newVariables, functions)
      }
      method(valueGetter, leftLimit, rightLimit)
    }
  }
}